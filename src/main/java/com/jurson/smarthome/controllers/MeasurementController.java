package com.jurson.smarthome.controllers;

import com.jurson.smarthome.data.Measurement;
import com.jurson.smarthome.repository.MeasurementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RequestMapping("/api")
@RestController
@CrossOrigin("http://localhost:3000")
public class MeasurementController {

    @Autowired
    private MeasurementRepository measurementRepository;

    @PostMapping("/insert")
    public String insert(@RequestBody Measurement measurement) {
        System.out.println("/insert requested with" + measurement.getTemperature() + " and " + "humi" + measurement.getHumidity());
        measurement.setCreationDate(LocalDateTime.now());
        measurementRepository.save(measurement);
        return "Inserted properly";
    }

    @PostMapping("/save")
    public String insert(@RequestParam String temp, @RequestParam String humi) {
        return "ok";
    }

    @PostMapping("/savei")
    public String savei(@RequestBody Measurement measurement) {
        System.out.println("/savei requested with" + measurement.getTemperature() + " and " + "humi" + measurement.getHumidity());
        return "ok";
    }


    @GetMapping("/getAll")
    public List<Measurement> getAll() {
        return measurementRepository.getAllByOrderByCreationDateAsc();
    }

    @GetMapping("/getPage")
    public List<Measurement> getPage(@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size) {

        if ( page == null)
            page = 0;
        if ( size == null)
            size = 20;

        PageRequest of = PageRequest.of(page, size, new Sort(Sort.Direction.DESC, "creationDate"));
        return measurementRepository.findAll(of).getContent();
    }

    @GetMapping("/add")
    public String add(@RequestParam Float temp, @RequestParam Float humi) {
        return "/add REQUESTED SUCCESSFULLY";
    }

    @GetMapping("/get")
    public Measurement get(@RequestParam String id) {
        return measurementRepository.findById(id).get();
    }

    @DeleteMapping("/deleteAll")
    public String deleteAll() {
        measurementRepository.deleteAll();
        return "deleted";
    }

}
