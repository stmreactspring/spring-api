package com.jurson.smarthome.data;


import lombok.Data;

@Data
public class ParticulateMatter {

    private Integer pm1;

    private Integer pm2;

    private Integer pm10;
}
