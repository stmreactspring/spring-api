package com.jurson.smarthome.data;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document(collection = "Measurement")
@Data
public class Measurement {

    private String id;

    private LocalDateTime creationDate;

    private Float temperature;

    private Float humidity;

    private ParticulateMatter particulateMatter;
}
