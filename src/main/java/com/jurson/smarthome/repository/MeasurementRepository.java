package com.jurson.smarthome.repository;


import com.jurson.smarthome.data.Measurement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface MeasurementRepository extends MongoRepository<Measurement, String> {

    List<Measurement> getAllByOrderByCreationDateAsc();

    Page findAllOrderByCreationDate(Pageable page);

}
